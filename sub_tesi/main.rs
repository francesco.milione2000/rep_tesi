use std::{
    process,
    io
};

extern crate paho_mqtt as mqtt;



fn main() {

    // Creo cliente specificando ip e porta, esco in caso di errore
    let mut cli = mqtt::Client::new("tcp://localhost:1883").unwrap_or_else(|err| {
        println!("Errore creazione cliente: {:?}", err);
        process::exit(1);
    });

    // Inizializzo il consumer
    let rx = cli.start_consuming();

    // Creo le opzioni di connessione, volendo potevo mettere qui ip e porta
    let conn_opts = mqtt::ConnectOptions::new();

    // Mi connetto, in caso di errore esco
    if let Err(e) = cli.connect(conn_opts) {
        println!("Errore connessione:{:?}", e);
        process::exit(1);
    }

    // Sono connesso
    let mut leggi = String::new();
    println!("Connessione stabilita, inserisci il topic:");
    io::stdin().read_line(&mut leggi).unwrap();
    let topic = leggi.trim_end();
    println!("Topic registrato: [{}]\n", topic);

    if let Err(e) = cli.subscribe(&topic, 0 as i32) {
        println!("Errore iscrizione al topic: {:?}", e);
        process::exit(1);
    }

    println!("Attendo messaggi...");
    for msg in rx.iter() {
        if let Some(msg) = msg {
            println!("{}", msg);
        }
        else if !cli.is_connected() {
            break;
        }
    }

    // Mi disconnetto
    if cli.is_connected() {
        println!("Disconnessione");
        cli.unsubscribe(&topic).unwrap();
        cli.disconnect(None).unwrap();
    }
    println!("Fine");
}