extern crate paho_mqtt as mqtt;

use std::process;
use std::io;

fn main() {
    // Creo cliente specificando ip e porta, esco in caso di errore
    let cli = mqtt::AsyncClient::new("tcp://localhost:1883").unwrap_or_else(|err| {
        println!("Errore creazione cliente: {}", err);
        process::exit(1);
    });

    // Creo le opzioni di connessione, volendo potevo mettere qui ip e porta
    let conn_opts = mqtt::ConnectOptions::new();

    // Mi connetto, in caso di errore esco
    if let Err(e) = cli.connect(conn_opts).wait() {
        println!("Errore connessione: {:?}", e);
        process::exit(1);
    }

    // Sono connesso
    let mut leggi = String::new();
    println!("Connessione stabilita, inserisci il topic:");
    io::stdin().read_line(&mut leggi).unwrap();
    let topic = leggi.trim_end();
    println!("Topic registrato: [{}]\n", topic);
    

    // Leggo da tastiera e invio messaggi
    println!("Inserisci messaggio (q per terminare)");
    let mut input = String::new();

    io::stdin().read_line(&mut input).unwrap();

    while input.trim().to_lowercase() != "q" {

        let msg = mqtt::Message::new(topic.clone(), input.clone().trim_end(), 0);
        let tok = cli.publish(msg);
        if let Err(e) = tok.wait() {
            println!("Errore invio messaggio: {:?}", e);
        }

        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        
    }
    


    // Mi disconnetto
    let tok = cli.disconnect(None);
    tok.wait().unwrap();
}